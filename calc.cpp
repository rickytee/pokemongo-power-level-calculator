#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>


using namespace std; 


int simulate( 
    int pokemons, 
    int candies, 
    int evolvereq
    )
{
    cout << "Evolve simulation: " << endl; 

    int total_evolved = 0; 

    while ( pokemons > 0 && candies >= evolvereq )
    {
        auto evolved    = candies / evolvereq; 

        candies         = candies % evolvereq; 

        candies         += evolved; 

        pokemons        -= evolved; 

        total_evolved   += evolved; 

        if ( ( candies < evolvereq ) 
        && ( ( pokemons + candies ) > evolvereq ) )
        {
            auto offset = evolvereq - candies; 

            pokemons    -= offset; 
            candies     += offset; 
        }
    }

    cout << "   Total evolved : " << total_evolved << endl; 
    cout << "   Candies left  : " << candies << endl; 
    cout << "   Pokemons left : " << pokemons << endl; 
}


struct Pokemon
{
    Pokemon( string name, int factor, int pokemons, int candies, int evolves = 0 ) :
        name( name ),
        factor( factor ),
        pokemons( pokemons ),
        candies( candies )
    { }

    string  name;
    int     factor;
    int     pokemons;
    int     candies;

    int     to_sell;
    int     to_evolve;
};


class PowerLevelAdjuster
{
public:

    float calculateGeometricFactor(
        int evolvereq,
        int candies
        )
    {
        float r = ( 1.0f / evolvereq );
        int n   = log( candies ) / log( evolvereq );

        return ( 1.0f - pow( r, n ) ) / ( 1 - r );
    }

    int calculatePossibleEvolve(
        Pokemon &pokemon
        )
    {
        int factor = pokemon.factor;
        int candies = pokemon.candies;
        int pokemons = pokemon.pokemons;

        int n = log( candies ) / log( factor );
        auto k = calculateGeometricFactor( factor, pokemons + candies );

        auto total_candies = candies * k;
        auto max_evolve = total_candies / factor;
        max_evolve += 0.001f;   ///< Precision

        return min( ( int ) max_evolve, pokemons );
    }

    float calculateToSell(
        Pokemon &pokemon
        )
    {
        int pokemons    = pokemon.pokemons;
        int candies     = pokemon.candies;
        int evolvereq   = pokemon.factor;

        auto k = calculateGeometricFactor( evolvereq, pokemons + candies );
//        cout << "Geometric factor  = " << k << endl;

        float up = ( ( float ) pokemons - ( float ) candies * k / evolvereq );
        float low = ( k / evolvereq + 1 );
        float to_sell = up / low;

        // Negative to sell means we don't have enough pokemons to use up all candies,
        // in which case don't sell any.
        return to_sell > 0 ? to_sell : 0;
    }
};


vector< Pokemon > processInput(
    string filename
    )
{
    ifstream infile( filename );

    vector< Pokemon > list;

    string line;
    while ( getline( infile, line ) )
    {
        istringstream iss( line );

        string name;
        int factor;
        int pokemons;
        int candies;

        iss >> name >> factor >> pokemons >> candies;

        list.emplace_back( name, factor, pokemons, candies );
    }

    return list;
}


int main()
{
    int pokemons    = 44;
    int candies     = 314;
    int factor      = 12;

    auto list = processInput( "input.txt" );

    PowerLevelAdjuster  adjuster;

    for ( auto & pokemon : list )
    {
        auto to_sell = adjuster.calculateToSell( pokemon );
        pokemon.to_sell    = to_sell;

        auto copy = pokemon;
        copy.pokemons   -= to_sell;
        copy.candies    += to_sell;

        auto to_evolve = adjuster.calculatePossibleEvolve( copy );
        pokemon.to_evolve  = to_evolve;
    }

    int total_evolve = 0;

    for ( auto & pokemon : list )
    {
        cout << pokemon.name;
        cout << ": Keep -> " << pokemon.pokemons - pokemon.to_sell;
        cout << ", Sell -> " << pokemon.to_sell;
        cout << ", Evolve -> " << pokemon.to_evolve;
        cout << endl;

        total_evolve += pokemon.to_evolve;
    }

    cout << endl;

    cout << "Total evolved -> " << total_evolve << endl;

////     cout << "Enter number of specified pokemons: " << endl;
//
//    cout << "Before Processing: " << endl;
//    cout << "   Pokemons    : " << pokemons << endl;
//    cout << "   Candies     : " << candies << endl;
//
//    cout << endl;
//
//    int to_sell     = calculateToSell( pokemons, candies, factor );
//    int to_evolve   = calculatePossibleEvolve( pokemons - to_sell, candies + to_sell, factor );
//
//    cout << "Instruction:" << endl;
//    cout << "   Sell        : " << to_sell << endl;
//    cout << "   Evolve      : " << to_evolve << endl;
//
//    to_sell = (int) to_sell;
//
//    cout << endl;
//
//    cout << "Before Power Level: " << endl;
//    cout << "   Pokemons    : " << pokemons - to_sell << endl;
//    cout << "   Candies     : " << candies + to_sell << endl;
//
//    cout << endl;
//
//    auto pokemons_sim = pokemons - to_sell;
//    auto candies_sim = candies + to_sell;
//
//    simulate( pokemons_sim, candies_sim, factor );
}
